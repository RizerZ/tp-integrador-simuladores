﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crawler : MonoBehaviour
{
    public GameObject trigger;

    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Run()
    {
        GetComponent<Animation>().Play("crawl_fast");
        AudioManager.instance.PlaySound("susto1");
        StartCoroutine(GameManager.instance.Photo());
    }
    public void Jump()
    {
        GetComponent<Animation>().Play("pounce");
        StartCoroutine(Deshabilitar());
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("TriggerCrawler"))
        {
            other.gameObject.SetActive(false);
            Jump();
        }
    }

    IEnumerator Deshabilitar()
    {
        yield return new WaitForSeconds(1.5f);
        this.gameObject.SetActive(false);
    }
}
