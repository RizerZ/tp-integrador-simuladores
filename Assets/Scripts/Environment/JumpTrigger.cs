﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpTrigger : MonoBehaviour
{
    public GameObject player;
    public GameObject jumpCam;
    public GameObject flashImg;
    // Start is called before the first frame update
    public void Scream()
    {
        AudioManager.instance.PlaySound("scream");
        jumpCam.SetActive(true);
        player.SetActive(false);
        flashImg.SetActive(true);
        StartCoroutine(GameManager.instance.Photo());
        StartCoroutine(EndJump());
    }

    IEnumerator EndJump()
    {
        yield return new WaitForSeconds(2f);
        player.SetActive(true);
        jumpCam.SetActive(false);
        flashImg.SetActive(false);
        StartCoroutine(PostProcessingManager.instance.blurred());
    }
        
}
