﻿using System.Collections;
using System.Runtime.Remoting.Messaging;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Luces : MonoBehaviour
{
    Light luz;
    public float maxEspera;
    public float minEspera;
    public int roundF;
    private bool coroutineActivated=false;
    float timer;
    public GameObject emtpyWithAudioSource;
    // Start is called before the first frame update

    private void Awake()
    {
        luz = GetComponentInChildren<Light>();
        //audio = GetComponent<AudioSource>();
    }
    void Start()
    {
        

    }

    private void Update()
    {
        if(roundF<=GameManager.instance.CurrentRound()&&!coroutineActivated)
        {
            StartCoroutine(FallaLuz());
            coroutineActivated = true;
        }
        
    }

    // Update is called once per frame
    IEnumerator FallaLuz()
    {
        while (true)
        {
            luz.enabled = true;
            timer = Random.Range(minEspera, maxEspera);
            yield return new WaitForSeconds(timer);
            luz.enabled = false;
            yield return new WaitForSeconds(Random.Range(0.2f, 0.8f));
            luz.enabled = true;
            luz.intensity = Random.Range(1f, 2f);
            SonarAudio();
            for (int i = 0; i < 5; i++)
            {
                yield return new WaitForSeconds(Random.Range(0.1f, 0.4f));
                luz.intensity = Random.Range(0.5f, 2f);
            }
            yield return new WaitForSeconds(Random.Range(0.3f, 1));
            luz.intensity = 2.5f;
        }
    }
    
    void SonarAudio()
    {
        emtpyWithAudioSource.GetComponent<AudioSource>().volume = (Random.Range(0.1f, 0.5f));
        emtpyWithAudioSource.GetComponent<AudioSource>().Play();
    }
}
