﻿using System.Collections;
using UnityEngine;

public class Relampagos : MonoBehaviour
{
    Light luz;
    public float minEspera;
    public float maxEspera;
    public bool option;
    // Start is called before the first frame update

    void Start()
    {
        luz = GetComponent<Light>();
        StartCoroutine(Prueba());

    }
    IEnumerator Prueba()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(minEspera, maxEspera));
            if(option)
            {
                AudioManager.instance.ReproducirSonidoR1(Random.Range(1, 3).ToString());
                
            }
            else
            {
                AudioManager.instance.ReproducirSonidoR2(Random.Range(1, 3).ToString());
            }
            //GestorAudio.instancia.ReproducirSonidoR2(Random.Range(4, 6).ToString());
            //ControladorPostProcessing.instancia.aberration.active = true;
            yield return new WaitForSeconds(Random.Range(0.1f, 0.4f));
            luz.enabled = true;
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.2f));
            luz.intensity = UnityEngine.Random.Range(2f, 5f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.4f));
            luz.intensity = UnityEngine.Random.Range(0, 2.5f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.3f));
            luz.intensity = UnityEngine.Random.Range(0.5f, 2f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.3f));
            luz.intensity = UnityEngine.Random.Range(0.2f, 2.5f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.2f));
            luz.intensity = 0.01f;
            yield return new WaitForSeconds(Random.Range(0.2f, 0.6f));
            luz.intensity = Random.Range(4f, 7f);
            luz.enabled = false;
            yield return new WaitForSeconds(Random.Range(1f, 3f));
            luz.enabled = true;
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.2f));
            luz.intensity = UnityEngine.Random.Range(1f, 4f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.4f));
            luz.intensity = UnityEngine.Random.Range(0, 3f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.3f));
            luz.intensity = UnityEngine.Random.Range(0.5f, 3f);
            luz.enabled = false;
            //ControladorPostProcessing.instancia.aberration.active = false;


        }
    }

    public IEnumerator Prueba(float min, float max)
    {
            yield return new WaitForSeconds(UnityEngine.Random.Range(min, max));
            if (option)
            {
                AudioManager.instance.ReproducirSonidoR1(Random.Range(1, 3).ToString());
            }
            else
            {
                AudioManager.instance.ReproducirSonidoR2(Random.Range(1, 3).ToString());
            }
            //GestorAudio.instancia.ReproducirSonidoR2(Random.Range(4, 6).ToString());
            //ControladorPostProcessing.instancia.aberration.active = true;
            yield return new WaitForSeconds(Random.Range(0.1f, 0.4f));
            luz.enabled = true;
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.2f));
            luz.intensity = UnityEngine.Random.Range(2f, 5f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.4f));
            luz.intensity = UnityEngine.Random.Range(0, 2.5f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.3f));
            luz.intensity = UnityEngine.Random.Range(0.5f, 2f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.3f));
            luz.intensity = UnityEngine.Random.Range(0.2f, 2.5f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.2f));
            luz.intensity = 0.01f;
            yield return new WaitForSeconds(Random.Range(0.2f, 0.6f));
            luz.intensity = Random.Range(4f, 7f);
            luz.enabled = false;
            yield return new WaitForSeconds(Random.Range(1f, 3f));
            luz.enabled = true;
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.2f));
            luz.intensity = UnityEngine.Random.Range(1f, 4f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, 0.4f));
            luz.intensity = UnityEngine.Random.Range(0, 3f);
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.05f, 0.3f));
            luz.intensity = UnityEngine.Random.Range(0.5f, 3f);
            luz.enabled = false;
            //ControladorPostProcessing.instancia.aberration.active = false;
    }
}

