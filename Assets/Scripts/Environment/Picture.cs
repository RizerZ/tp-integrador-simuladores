﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Picture : MonoBehaviour
{
    private MeshRenderer meshRenderer;
    private Vector3 position;
    private Quaternion rotation;
    public bool texture = false;
    public bool cuadro = false;
    // Start is called before the first frame update

    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        position = new Vector3(this.transform.parent.position.x, this.transform.parent.transform.position.y, this.transform.parent.transform.position.z);
        rotation = new Quaternion(this.transform.parent.rotation.x, this.transform.parent.rotation.y, this.transform.parent.rotation.z,transform.rotation.w);
    }
    void Start()
    {
        /*
        if(WebcamCapture.instance.webCam)
        {
            SetTextureInPicture();
        }*/
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameManager.instance.puzzle!=null)
        {
            if (WebcamCapture.instance.webCam && !texture && GameManager.instance.puzzle.GetComponent<Puzzle>().puzzleSolved && !texture)
            {
                SetTextureInPicture();
                texture = true;
            }

            if (GameManager.instance.puzzle.GetComponent<Puzzle>().puzzleSolved && !cuadro)
            {
                transform.parent.GetComponent<Rigidbody>().useGravity = false;
                this.transform.parent.position = position;
                this.transform.parent.rotation = rotation;
                cuadro = true;

            }
        }
        
    }


    private void SetTextureInPicture()
    {
        if(GetFiles.instance.GetTextureFromPhoto()!=null)
        {
            meshRenderer.material.SetTexture("_BaseMap", GetFiles.instance.GetTextureFromPhoto());
        }
        
    }
}