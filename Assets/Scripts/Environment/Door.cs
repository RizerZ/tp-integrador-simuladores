﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    // Start is called before the first frame update

    public void PuertaCerrando()
    {
        AudioManager.instance.PlaySound("puertaCerrando");
        GameManager.instance.wind.GetComponent<Animator>().enabled = true;
        Invoke("StopWind",5f);
    }

    public void StopWind()
    {
        GameManager.instance.wind.SetActive(false);
    }
}
