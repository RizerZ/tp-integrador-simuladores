﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDoorController : MonoBehaviour
{

    [SerializeField]
    Animator myDoor = null;
    [SerializeField] 
    bool openTrigger = false;
    [SerializeField]
    bool closeTrigger = false;

    [SerializeField]
    string openDoor = "OpenDoor";

    [SerializeField]
    string closeDoor = "CloseDoor";




    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if(openTrigger)
            {
                myDoor.Play(openDoor,0,0f);
                AudioManager.instance.PlaySound("puertaAbriendo");
                gameObject.SetActive(false);
            }

            else if(closeTrigger)
            {
                GameManager.instance.crawler.SetActive(true);
                myDoor.Play(closeDoor, 0, 0f);
                Camera.main.gameObject.GetComponent<BoxCollider>().enabled = true;
                gameObject.SetActive(false);
            }
        }
    }
}
