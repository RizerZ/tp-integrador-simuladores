﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Opener : MonoBehaviour
{
    [SerializeField]
    Animator door = null;
    
    bool isOpen=false;
    [Range (0,2)]
    public int key = 0;

    private bool firstDoor = false;

    private void Update()
    {
        door.SetBool("isOpen", isOpen);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && Input.GetKeyDown(KeyCode.E)&&key<=GameManager.instance.key)
        {
            isOpen = !isOpen;
            if (isOpen)
            {
                AudioManager.instance.PlaySound("openDoor");
            }
            if(!isOpen)
            {
                AudioManager.instance.PlaySound("closeDoor", 20000);
            }
            
            if(!firstDoor)
            {
                GameManager.instance.roundF++;
                firstDoor = true;
            }
        }
        else if(other.CompareTag("Player")&&Input.GetKeyDown(KeyCode.E)&&key>GameManager.instance.key)
        {
            GameManager.instance.hints.text = "I need the key";
            StartCoroutine(GameManager.instance.ClearText(GameManager.instance.hints));
            AudioManager.instance.PlaySound("locked");
        }
    }
}

