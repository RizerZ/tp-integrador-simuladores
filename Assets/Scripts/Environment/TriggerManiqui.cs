﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerManiqui : MonoBehaviour
{
    public GameObject oldManiqui;
    public GameObject newManiqui;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            oldManiqui.SetActive(false);
            newManiqui.SetActive(true);
        }
    }
}
