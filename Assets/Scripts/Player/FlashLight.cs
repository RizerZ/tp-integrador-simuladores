﻿using System.Collections;
using UnityEngine;

public class FlashLight : MonoBehaviour
{
    // Start is called before the first frame update
    Light luz;
    public float minEspera;
    public float maxEspera;
    AudioSource sonido;
    // Start is called before the first frame update

    void Start()
    {
        luz = GetComponent<Light>();
        StartCoroutine(FallaLinterna());
        sonido=GetComponent<AudioSource>();

    }
    IEnumerator FallaLinterna()
    {
        while (true)
        {
            
            yield return new WaitForSeconds(Random.Range(minEspera, maxEspera));
            luz.enabled = false;
            yield return new WaitForSeconds(Random.Range(0.2f, 0.8f));
            luz.enabled = true;
            luz.intensity = Random.Range(0f, 6f);
            sonido.Play();
            for(int i =0; i<5;i++)
            {
                yield return new WaitForSeconds(Random.Range(0.1f, 0.4f));
                luz.intensity = Random.Range(0f, 8f);
            }
            yield return new WaitForSeconds(Random.Range(0.3f, 1));
            luz.intensity = 11;
        }
    }
}
