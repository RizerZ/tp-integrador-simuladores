﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour//clase que controla al jugador
{
    #region Propiedades


    public float speed = 8.0f;//propiedad para movimiento
    Rigidbody rb;//rigidbody del personaje
    public float jumpForce = 20;//propiedad para salto


    int currentJump = 0;//propiedad para regular la cantidad de saltos
    bool isFloor = true;//propiedad para saber si está en el suelo o no y de esta forma controlar los saltos
    int maxJump = 1;//máximo de saltos
    Vector3 temp = new Vector3();
    public GameObject soundBateria;
    public GameObject bateria;
    public Light lightBateria;
    public Light lightOrgano;
    public GameObject soundOrgano;
    public GameObject musicOrgano;
    public bool battery = false;
    public bool organ = false;
    public bool ball = false;
    #endregion
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;//saco el cursor para que no moleste
        
    }

    void Update()
    {
        
        Movement();//llamo al método movimiento en cada frame
        Jump();//llamo al método salto en cada frame
        

    }

    void Movement()//método movimiento
    {
        temp = rb.transform.position;
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * speed;
        float movimientoCostados = Input.GetAxis("Horizontal") * speed;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        if (isFloor == true && Input.GetKey("up")==true)
        {
            temp = rb.transform.position;
            Debug.Log("Caminando");
        }

    }
    void Jump()//método salto
    {


        if (Input.GetButtonDown("Jump") && (isFloor || maxJump > currentJump))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isFloor = false;
            currentJump++;
        }
    }


    private void OnCollisionEnter(Collision col)//detector de colisiones
    {
        isFloor = false;
        currentJump = 0;

    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Bateria")&&!battery)
        {
            StartCoroutine(Luz(lightBateria, soundBateria));
            StartCoroutine(GameManager.instance.Photo());
            battery = true;
        }
        if(other.gameObject.CompareTag("Organo")&&!organ)
        {
            StartCoroutine(Luz(lightOrgano, soundOrgano));
            StartCoroutine(GameManager.instance.Photo());
            organ = true;

        }
        if(other.gameObject.CompareTag("Ball")&&!ball)
        {
            if(GameManager.instance.craw)
            {
                GameManager.instance.ball.SetActive(true);
                ball = true;
            }            
        }
        if(other.gameObject.CompareTag("TriggerMusicBox"))
        {
            GameManager.instance.musicBox.GetComponent<Animator>().enabled = true;
            GameManager.instance.musicBox.GetComponent<AudioSource>().Play();
            other.gameObject.SetActive(false);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("FixCrawler"))
        {
            Camera.main.GetComponent<BoxCollider>().enabled = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("FixCrawler"))
        {
            Camera.main.GetComponent<BoxCollider>().enabled = false;
        }
    }

    IEnumerator Luz(Light luz, GameObject instrumento)
    {
        bool a = true;
        while (a == true)
        {
            luz.enabled = true;
            luz.intensity = 0;
            //ControladorPostProcessing.instancia.film.active = true;
            //ControladorPostProcessing.instancia.aberration.active = true;
            //ControladorPostProcessing.instancia.bloom.active = true;
            for (int i = 1; i < 8; i++)
            {
                yield return new WaitForSeconds(0.2f);
                luz.intensity += 3;
                if(i==3)
                {
                    instrumento.GetComponent<AudioSource>().Play();
                    bateria.GetComponent<Animator>().Play("Bateria1");
                }
            }
            yield return new WaitForSeconds(1f);
            luz.enabled = false;
            //ControladorPostProcessing.instancia.aberration.active = false;
            //ControladorPostProcessing.instancia.film.active = false;
            //ControladorPostProcessing.instancia.bloom.active = false;
            a = false;
        }
    }
}

   

