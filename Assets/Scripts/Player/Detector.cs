﻿using System.Collections;
using UnityEngine;

public class Detector : MonoBehaviour
{
    //public Text asd;
    // Start is called before the first frame update
    public PlayerController player;
    void Update()
    {
            Detect();
    }

    void Detect()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {

            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            if ((Physics.Raycast(ray, out hit) == true) && (hit.distance < 5) && (hit.collider.CompareTag("Battery")))
            {
                hit.collider.gameObject.SetActive(false);
                FindObjectOfType<PlayerFeatures>().batteryCharge += 30f;
            }

            if ((Physics.Raycast(ray, out hit) == true) && (hit.distance < 5) && (hit.collider.CompareTag("Seleccionable")) /*&& asd.enabled == false*/)
            {
                //asd.text = hit.collider.name;
                //asd.enabled = true;
                //Invoke("Ocultar", 3);
                Debug.Log(hit.collider.name);
                hit.collider.gameObject.SetActive(false);
                GameManager.instance.roundF++;
                AudioManager.instance.PlaySound("llaves");
                if(GameManager.instance.key==1)
                {
                    GameManager.instance.gameObject.GetComponent<JumpTrigger>().Scream();
                    GameManager.instance.key++;
                    GameManager.instance.player.transform.position = new Vector3(-6, 4, 36);
                    GameManager.instance.keys[GameManager.instance.key-1].gameObject.SetActive(true);
                    GameManager.instance.Screens();

                }
                else
                {
                    GameManager.instance.key++;
                    GameManager.instance.keys[GameManager.instance.key-1].gameObject.SetActive(true);
                }
                
            }
            if((Physics.Raycast(ray, out hit) == true) && (hit.distance < 5) && (hit.collider.CompareTag("Cuadro")))
            {
                hit.collider.gameObject.GetComponent<Rigidbody>().useGravity = true;
                hit.collider.gameObject.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * -UnityEngine.Random.Range(1.5f, 3f),ForceMode.Impulse);
                AudioManager.instance.PlaySound("mueveCuadro");
                StartCoroutine(CaeCuadro());
            }
            if((Physics.Raycast(ray,out hit)==true)&&(hit.distance<5)&&(hit.collider.CompareTag("CajaFuerte")))
            {

                GameManager.instance.puzzle.SetActive(true);
                AudioManager.instance.PlaySound("cajaFuerte");
                if(!GameManager.instance.puzzleOpened)
                {
                    //StartCoroutine(GameManager.instance.Mic());
                    StartCoroutine(GameManager.instance.Alarm());
                    StartCoroutine(GameManager.instance.Photo());
                    player.musicOrgano.GetComponent<AudioSource>().Play();
                    StartCoroutine(GameManager.instance.Sounds());
                    for (int i = 0; i <= UnityEngine.Random.Range(500, 900); i++)
                    {
                        GameManager.instance.puzzle.GetComponent<Puzzle>().Shuffle();
                        GameManager.instance.puzzleOpened = true;
                    }
                }
                
            }
            if ((Physics.Raycast(ray, out hit) == true) && (hit.distance < 5) && (hit.collider.CompareTag("Reloj")))
            {
                GameManager.instance.clock.GetComponent<Animator>().enabled = false;
                GameManager.instance.clock.GetComponent<AudioSource>().Stop();
                GameManager.instance.maniqui.SetActive(true);
            }

            if ((Physics.Raycast(ray, out hit) == true) && (hit.distance < 5) && (hit.collider.CompareTag("MusicBox")))
            {
                GameManager.instance.musicBox.GetComponent<Animator>().enabled = !GameManager.instance.musicBox.GetComponent<Animator>().enabled;
                if(GameManager.instance.musicBox.GetComponent<AudioSource>().isPlaying)
                {
                    GameManager.instance.musicBox.GetComponent<AudioSource>().Pause();
                }
                else
                {
                    GameManager.instance.musicBox.GetComponent<AudioSource>().UnPause();
                }
                
                
            }

                if ((Physics.Raycast(ray, out hit) == true) && (hit.distance < 5) && (hit.collider.CompareTag("Nota")))
            {
                AudioManager.instance.PlaySound("hoja");    
                string x=hit.collider.gameObject.GetComponent<Hints>().text;
                GameManager.instance.hints.text = x;
                hit.collider.gameObject.SetActive(false);
                Debug.Log(x);
                StartCoroutine(GameManager.instance.ClearText(GameManager.instance.hints, true));
                GameManager.instance.notes++;
                GameManager.instance.NotesImg[GameManager.instance.notes-1].color = Color.white;
            }
            else
            {
                Debug.Log(hit.collider.gameObject.name.ToString());
            }

        }
    }
    IEnumerator CaeCuadro()
    {
        yield return new WaitForSeconds(0.7f);
        AudioManager.instance.PlaySound("caeCuadro");
        yield return new WaitForSeconds(1.3f);
        AudioManager.instance.PlaySound("caeCuadro");
    }

}

