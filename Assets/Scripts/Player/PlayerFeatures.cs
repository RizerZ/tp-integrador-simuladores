﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFeatures : MonoBehaviour
{
    public Light flashlight;
    public Light pointlight;

    public float batteryCharge = 100.0f;
    public float batteryChargeRate = 0.01f;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F))
        {
            flashlight.enabled = !flashlight.isActiveAndEnabled;
            pointlight.enabled = flashlight.enabled;
            if(batteryCharge<=0)
            {
                GameManager.instance.batteries.text = "I need batteries";
                StartCoroutine(GameManager.instance.ClearText(GameManager.instance.batteries));
            }
        }

        if(flashlight.isActiveAndEnabled)
        {
            batteryCharge -= batteryChargeRate * Time.deltaTime;
        }

        if(batteryCharge<=0)
        {
            flashlight.enabled = false;
            pointlight.enabled = flashlight.enabled;
        }

        batteryCharge = Mathf.Clamp(batteryCharge, 0.0f, 100.0f);
    }
}
