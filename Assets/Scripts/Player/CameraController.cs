﻿using UnityEngine;

public class CameraController : MonoBehaviour//camara en tercera persona de lejos con clamp
{
    Vector2 mouselook;
    Vector2 smoothV;

    public float sensibility = 3.0f;
    public float smooth = 2.0f;

    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = this.transform.parent.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameManager.instance.puzzle.activeSelf)
        {  
            var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));

            md = Vector2.Scale(md, new Vector2(sensibility * smooth, sensibility * smooth));
            smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smooth);
            smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smooth);

            mouselook += smoothV;

            mouselook.y = Mathf.Clamp(mouselook.y, -80f, 80f);

            transform.localRotation = Quaternion.AngleAxis(mouselook.y*-1, Vector3.right);
            player.transform.localRotation = Quaternion.AngleAxis(mouselook.x, player.transform.up);
            Cursor.visible = false;
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.CompareTag("TriggerCrawler"))
        {
            GameManager.instance.fixCrawler.SetActive(false);
            GameManager.instance.crawler.GetComponent<Crawler>().Run();
            GameManager.instance.UnRelampago();
            GetComponent<BoxCollider>().enabled = false;
            GameManager.instance.craw = true;
            
        }
    }
}
