﻿using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puzzle : MonoBehaviour
{
    public NumberBox boxPrefab;
    public NumberBox[,] boxes = new NumberBox[4, 4];
    public Sprite[] sprites;
    public NumberBox[,] start = new NumberBox[4, 4];
    public bool puzzleSolved=false;
    // Start is called before the first frame update

    private void Awake()
    {
        Init();
        for (int i = 0; i < Random.Range(1, 10); i++)
        {
            Shuffle();
        }
        this.gameObject.SetActive(false);
    }

    private void Start()
    {
        GameManager.instance.puzzle = this.gameObject;
    }

    private void Update()
    {
        if(GameManager.instance.puzzleOpened)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        
        if(check())
        {
            Debug.Log("win");
            puzzleSolved = true;
        }

    }

    private void Init()
    {
        int n = 0;
        for (int y=3;y>=0;y--)
            for(int x=0;x<4;x++)
            {
                NumberBox box = Instantiate(boxPrefab, new Vector2(x*100, y*100), Quaternion.identity);
                box.transform.SetParent(this.transform);
                box.Init(x, y, n + 1, sprites[n], ClickToSwap);
                boxes[x, y] = box;
                start[x, y] = new NumberBox();
                start[x, y].index = n+1;
                n++;
            }
    }
    void Swap(int x,int y, int dx, int dy)
    {

        var from = boxes[x, y];
        var target = boxes[x + dx, y + dy];

        boxes[x, y] = target;
        boxes[x + dx, y + dy] = from;

        from.UpdatePos(x + dx, y + dy);
        target.UpdatePos(x, y);

        
    }

    int getDx(int x, int y)
    {
        if(x<3&&boxes[x+1,y].IsEmpty())
        {
            AudioManager.instance.PlaySound("move");
            return 1;
        }
        if(x>0&&boxes[x-1,y].IsEmpty())
        {
            AudioManager.instance.PlaySound("move");
            return -1;
        }
        else
        {
            return 0;
        }
        
    }

    int getDy(int x, int y)
    {
        if (y < 3 && boxes[x, y+1].IsEmpty())
        {
            AudioManager.instance.PlaySound("move");
            return 1;

        }
        if (y > 0 && boxes[x, y-1].IsEmpty())
        {
            AudioManager.instance.PlaySound("move");
            return -1;
        }
        else
        {
            return 0;
        }
    }

    public void Shuffle()
    {
        for(int i=0; i<4;i++)
        {
            for(int j=0;j<4;j++)
            {
                if(boxes[i,j].IsEmpty())
                {
                    Vector2 pos = getValidMove(i, j);

                    Swap(i, j, (int)pos.x, (int)pos.y);
                }
            }
        }
    }

    Vector2 getValidMove(int x,int y)
    {
        Vector2 pos = new Vector2();

        do
        {
            int n = Random.Range(0, 4);
            if (n == 0)
            {
                pos = Vector2.left;
            }
            else if (n == 1)
            {
                pos = Vector2.right;
            }
            else if (n == 2)
            {
                pos = Vector2.up;
            }
            else
            {
                pos = Vector2.down;
            }
        } while (!(isValidRange(x + (int)pos.x) && isValidRange(y + (int)pos.y))||isRepeatMove(pos));


        lastMove = pos;
        return pos;
    }
    void ClickToSwap(int x, int y)
    {

        int dx = getDx(x, y);
        int dy = getDy(x, y);
        Swap(x, y, dx, dy);
    }

    bool isValidRange(int n)
    {
        return n >= 0 && n <= 3;
    }

    bool isRepeatMove(Vector2 pos)
    {
        return pos * -1 == lastMove;
    }

    private Vector2 lastMove;

    bool check()
    {
        for (int y = 3; y >= 0; y--)
            for (int x = 0; x < 4; x++)
            {
                if(boxes[x,y].index==start[x,y].index)
                {

                }
                else
                {
                    return false;
                }
            }
        return true;
    }
}
