﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberBox : MonoBehaviour
{
    public int index = 0;
    int x = 0;
    int y = 0;
    private Action<int, int> swapFunc = null;

    public void Init(int i, int j, int index, Sprite sprite, Action<int,int>swapFunc)
    {
        this.index = index;
        this.GetComponent<Image>().sprite = sprite;
        UpdatePos(i, j);
        this.swapFunc = swapFunc;
    }

    public void UpdatePos(int i, int j)
    {
        x = i;
        y = j;
        //this.gameObject.transform.localPosition = new Vector2(i*100, j*100);
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        float elapsedTime = 0;
        float duration = 0.2f;
        Vector2 start = this.gameObject.transform.localPosition;
        Vector2 end = new Vector2(x*100, y*100);
        

        while (elapsedTime<duration)
        {
            this.gameObject.transform.localPosition = Vector2.Lerp(start,end,(elapsedTime/duration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }

        this.gameObject.transform.localPosition = end;
    }

    public bool IsEmpty()
    {
        return index == 16;
    }
    public void Movement()
    {
        swapFunc(x, y);
    }
}
