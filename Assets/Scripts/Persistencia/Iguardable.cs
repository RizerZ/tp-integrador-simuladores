﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iguardable
{
    // Start is called before the first frame update
    object CapturarEstado();
    void RestaurarEstado(object estado);
}
