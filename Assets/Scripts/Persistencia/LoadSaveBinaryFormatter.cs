﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class LoadSaveBinaryFormatter : MonoBehaviour
{
    private string rutaArchivo => $"{Application.persistentDataPath}/save.txt";

    [ContextMenu("Guardar")]
    public void Guardar()
    {
        var estado = CargarArchivo();
        CapturarEstado(estado);
        GuardarArchivo(estado);

    }

    public void Cargar()
    {
        var estado = CargarArchivo();
        RestaurarEstado(estado);
    }

    private void RestaurarEstado(Dictionary<string, object> estado)
    {
        foreach (var guardable in FindObjectsOfType<EntidadGuardable>())
        {
            if (estado.TryGetValue(guardable.Id, out object value))
            {
                guardable.RestaurarEstado(value);
            }
        }
    }

    private void GuardarArchivo(object estado)
    {
        using (var Stream = File.Open(rutaArchivo, FileMode.Create))
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(Stream, estado);
        }
    }

    private void CapturarEstado(Dictionary<string,object> estado)
    {
        foreach (var guardable in FindObjectsOfType<EntidadGuardable>())
        {
            estado[guardable.Id] = guardable.CapturarEstado();
        }
    }

    private Dictionary<string,object> CargarArchivo()
    {
        if(!File.Exists(rutaArchivo))
        {
            return new Dictionary<string, object>();
        }
        using (FileStream stream=File.Open(rutaArchivo,FileMode.Open))
        {
            var formatter = new BinaryFormatter();
            return (Dictionary<string, object>)formatter.Deserialize(stream);
        }
    }
}
