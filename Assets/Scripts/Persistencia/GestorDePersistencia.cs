﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestorDePersistencia : MonoBehaviour
{

    public static GestorDePersistencia instancia;
    // Start is called before the first frame update
    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame

    public void GuardarEstadoJuego()
    {
        GameObject loadSaveBinaryFormatter = GameObject.Find("LoadSaveBinaryFormatter");
        LoadSaveBinaryFormatter script = (LoadSaveBinaryFormatter)loadSaveBinaryFormatter.GetComponent(typeof(LoadSaveBinaryFormatter));
        script.Guardar();
    }

    public void CargarEstadoJuego()
    {
        GameObject loadSaveBinaryFormatter = GameObject.Find("LoadSaveBinaryFormatter");
        LoadSaveBinaryFormatter script = (LoadSaveBinaryFormatter)loadSaveBinaryFormatter.GetComponent(typeof(LoadSaveBinaryFormatter));
        script.Cargar();
    }
}
