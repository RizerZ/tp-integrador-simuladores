﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntidadGuardable : MonoBehaviour
{
    [SerializeField] private string id = string.Empty;

    public string Id => id;
    [ContextMenu("Generar GUID")]
    private void GenerarGUID() => id = System.Guid.NewGuid().ToString();

    public object CapturarEstado()
    {
        var dicEstado = new Dictionary<string, object>();
        foreach (var guardable in GetComponents<Iguardable>())
        {
            dicEstado[guardable.GetType().ToString()] = guardable.CapturarEstado();
        }
        return dicEstado;
    }
    public void RestaurarEstado(object estado)
    {
        var dicEstado = (Dictionary<string, object>)estado;
        foreach (var guardable in GetComponents<Iguardable>())
        {
            string nombreDelTipoDeDato = guardable.GetType().ToString();
            if (dicEstado.TryGetValue(nombreDelTipoDeDato, out object value))
            {
                guardable.RestaurarEstado(value);
            }
        }
    }
}
