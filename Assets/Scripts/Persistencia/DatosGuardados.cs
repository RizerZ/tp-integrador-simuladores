﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatosGuardados : MonoBehaviour, Iguardable
{

    public Vector3 playerPosition;
    public int key;
    public bool load = false;
    public bool savedGame = false;
    public string playerName;

    public static DatosGuardados instance;
    // Start is called before the first frame update


    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        GestorDePersistencia.instancia.CargarEstadoJuego();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerPosition.y != 0)
        {
            savedGame = true;
        }

    }

    public object CapturarEstado()
    {
        return new SaveData
        {
            //position = playerPosition,
            key = key,
            x = playerPosition.x,
            y = playerPosition.y,
            z = playerPosition.z
        };
    }

    public void RestaurarEstado(object estado)
    {
        var saveData = (SaveData)estado;
        //playerPosition = saveData.position;
        
        key = saveData.key;
        playerPosition.x = saveData.x;
        playerPosition.y = saveData.y;
        playerPosition.z = saveData.z;
    }

    [Serializable]
    private struct SaveData
    {
        //public Vector3 position;
        public float x;
        public float y;
        public float z;
        public int key;
    }
}
