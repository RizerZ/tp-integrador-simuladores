﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Button newGame;
    public Button exit;
    public Button continu;
    public GameObject menuPrincipal;
    public GameObject warning;
    public GameObject controls;
    public GameObject input;
    public int warn;
    public InputField inpField;
    // Start is called before the first frame update

    private void Awake()
    {
        warn = PlayerPrefs.GetInt("value");
    }
    void Start()
    {
        
        if(warn == 0)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            AudioManager.instance.PauseAll();
            StartCoroutine(AudioManager.instance.Sounds());
            AudioManager.instance.PlaySound("menu");
            menuPrincipal.SetActive(false);
            warning.SetActive(true);
        }
        if(warn==1)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            AudioManager.instance.PauseAll();
            StartCoroutine(AudioManager.instance.Sounds());
            AudioManager.instance.PlaySound("menu");
            menuPrincipal.SetActive(true);
            warning.SetActive(false);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if(DatosGuardados.instance.savedGame)
        {
            continu.gameObject.SetActive(true);
        }
    }

    public void NewGame()
    {
        SceneManager.LoadScene(1);

    }

    public void Controls()
    {
        controls.gameObject.SetActive(true);
    }

    public void Exit()
    {
        Application.Quit();
        Debug.Log("Salió");
    }

    public void Continue()
    {
        DatosGuardados.instance.load = true;
        SceneManager.LoadScene(1);
        DatosGuardados.instance.playerName=PlayerPrefs.GetString("name");

    }

    public void Understand()
    {
        warning.SetActive(false);
        menuPrincipal.SetActive(true);
        PlayerPrefs.SetInt("value", 1);
    }
    
    public void OkControls()
    {
        controls.SetActive(false);
        input.SetActive(true);
    }

    public void OkInput()
    {
        if(inpField.text.Length>0)
        {
            DatosGuardados.instance.playerName = inpField.text;
            PlayerPrefs.SetString("name", inpField.text);
            //input.SetActive(false);
            NewGame();
        }
        
        
    }
}
