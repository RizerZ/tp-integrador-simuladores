﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour
{
    public Material matMouseOver;
    public Material original;
    // Start is called before the first frame update
    private void Awake()
    {
        original=GetComponent<Renderer>().material;
    }

    private void Update()
    {
        if(this.gameObject.CompareTag("Cuadro") && this.gameObject.GetComponent<Rigidbody>().useGravity)
        {
            GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
            Destroy(this);
        }
    }
    private void OnMouseOver()
    {
        
        if(this.gameObject.CompareTag("Cuadro"))
        {
            GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.grey);
            GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
        }
        if(this.gameObject.CompareTag("Nota"))
        {
            //GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.white);
            GetComponent<Renderer>().material = matMouseOver;
        }
    }

    private void OnMouseExit()
    {
        if(this.gameObject.CompareTag("Cuadro"))
        {
            GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
        }
        if(this.gameObject.CompareTag("Nota"))
        {
            GetComponent<Renderer>().material = original;
        }
        
    }
}
