﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessingManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static PostProcessingManager instance;

    public Volume volume;
    public ChromaticAberration aberration;
    public DepthOfField blur;
    public Vignette vignette;
    public FilmGrain film;
    public Bloom bloom;
    public MotionBlur mb;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        volume = GetComponent<Volume>();
        vignette = (Vignette)volume.profile.components[0];
        bloom = (Bloom)volume.profile.components[1];
        aberration = (ChromaticAberration)volume.profile.components[2];
        blur = (DepthOfField)volume.profile.components[3];
        film = (FilmGrain)volume.profile.components[4];
        mb = (MotionBlur)volume.profile.components[5];
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator blurred()
    {
        blur.active = true;
        yield return new WaitForSeconds(3);
        for (int i = 0; i < 214; i++)
        {
            blur.focalLength.value -= 1.4f;
            yield return new WaitForSeconds(Random.Range(0.05f, 0.1f));
        }
        blur.active = false;
    }

    public void ChangeBloom()
    {
        bloom.tint.Override(Color.red);
        bloom.intensity.value = 2;
    }
}

