﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Windows.WebCam;
using UnityScript.Steps;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int roundF;
    public int key = 0;//guardar
    public GameObject puzzle;
    public bool puzzleOpened = false;
    public GameObject player;//guardar position
    public Text puzzleWin;
    public RawImage[] keys;
    public Text hints;
    public GameObject crawler;
    public GameObject[] relampagos;
    public GameObject ball;
    public bool craw = false;
    public bool puzzleComplete = false;
    public GameObject PauseMenu;
    public CameraController cc;
    public bool isPaused = false;
    public GameObject[] screen;
    public Material matWebcam;
    public GameObject fadeOut;
    public List<float> valores;
    public int notes = 0;
    public RawImage[] NotesImg;

    public Text batteries;

    public GameObject noteAndText;
    public TextMeshProUGUI playerName;
    public TextMeshProUGUI hint1;
    public TextMeshProUGUI hint2;
    public Text text;

    public GameObject wind;

    public int ruido;
    public int count;

    public bool firstN = true;

    float cutoff = 0.3f;

    public GameObject clock;
    public GameObject musicBox;

    public GameObject maniqui;

    public GameObject fixCrawler;
    void Awake()
    {
        playerName.text = DatosGuardados.instance.playerName;
        if (instance == null)
        {
            instance = this;

        }
        valores = new List<float>();
        noteAndText.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        cc=FindObjectOfType<CameraController>();
        Resume();
        AudioManager.instance.PauseAll();
        Load();
        PostProcessingManager.instance.bloom.tint.Override(Color.white);
        StartCoroutine(Test2());
        playerName.ForceMeshUpdate(true);
        hint1.ForceMeshUpdate(true);
        hint2.ForceMeshUpdate(true);
        text.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (puzzle!=null)
        {
            if (Input.GetKeyDown(KeyCode.Escape) && !puzzle.activeSelf)
            {
                if (isPaused)
                {
                    Resume();
                    
                }
                else
                {
                    PauseGame();
                    
                }
            }
            if(Input.GetKeyDown(KeyCode.N)&&!isPaused)
            {
                text.gameObject.SetActive(false);
                noteAndText.SetActive(!noteAndText.activeSelf);
                if(firstN)
                {
                    StartCoroutine(WriteText(playerName,0));
                    firstN = false;
                }
                if(notes==1)
                {
                    StartCoroutine(WriteText(playerName, 0));
                    StartCoroutine(WriteText(hint1, 0f));
                }
                if(notes==2)
                {
                    StartCoroutine(WriteText(playerName, 0));
                    StartCoroutine(WriteText(hint1, 0f));
                    StartCoroutine(WriteText(hint2, 0.2f));
                }
                if(notes==3)
                {
                    StartCoroutine(WriteText(playerName, 0));
                    StartCoroutine(WriteText(hint1, 0f));
                    StartCoroutine(WriteText(hint2, 0.1f));
                }
                if(notes==4)
                {
                    StartCoroutine(WriteText(playerName, 0));
                    StartCoroutine(WriteText(hint1, 0f));
                    StartCoroutine(WriteText(hint2, 0));
                }
                

            }

            if (puzzle.activeSelf && Input.GetKeyDown(KeyCode.Escape))
            {
                puzzle.SetActive(false);
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

            if (puzzle.GetComponent<Puzzle>().puzzleSolved && !puzzleComplete)
            {
                puzzleWin.gameObject.SetActive(true);
                player.GetComponent<PlayerController>().musicOrgano.GetComponent<AudioSource>().Stop();
                AudioManager.instance.PlaySound("end");
                StopCoroutine(Sounds());
                puzzleComplete = true;
                PostProcessingManager.instance.ChangeBloom();
                StartCoroutine(FadeEnding());
                StopCoroutine(Test2());
            }
            /*
            if (puzzleOpened && !puzzle.gameObject.GetComponent<Puzzle>().puzzleSolved)
            {
                if (MicInputDb.MicLoudnessinDecibels < -170)
                {
                    //Debug.Log("Sin micrófono.");
                }
                else if (MicInputDb.MicLoudnessinDecibels > -150)
                {
                    if (MicInputDb.MicLoudnessinDecibels > -80 && UnityEngine.Random.Range(1f, 100f) > 85)
                    {
                        AudioManager.instance.PlaySound("sh");
                    }
                    else if (MicInputDb.MicLoudnessinDecibels < -135 && UnityEngine.Random.Range(1f, 100f) > 85)
                    {
                        AudioManager.instance.PlaySound("noTeEscucho");
                    }
                }
            }
            */
        }
    }

    public int CurrentRound()
    {
        return roundF;
    }

    public IEnumerator ClearText(Text text, bool N=false)
    {
        if(text.text.Length==0)
        {
            yield return new WaitForSeconds(0.5f);
            if (N)
            {
                GameManager.instance.text.gameObject.SetActive(true);
            }
        }
        else
        {
            yield return new WaitForSeconds(5.0f);

            text.text = " ";
            yield return new WaitForSeconds(0.5f);
            if (N)
            {
                GameManager.instance.text.gameObject.SetActive(true);
            }
        }
        
        
    }

    public void UnRelampago()
    {
        foreach (var elemento in relampagos)
        {
            StartCoroutine(elemento.GetComponent<Relampagos>().Prueba(0, 0));
        }
    }

    public IEnumerator Sounds()
    {
        while (!puzzle.GetComponent<Puzzle>().puzzleSolved)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(2.5f, 8f));
            AudioManager.instance.PlayRandomSound();
            yield return new WaitForSeconds(UnityEngine.Random.Range(2f, 15f));
            AudioManager.instance.PlayRandomSound();

        }
        
    }

    public IEnumerator Photo()
    {
        yield return new WaitForSeconds(0.6f);
        SavePhoto.instance.TakePhoto();
    }

    public IEnumerator Mic()
    {



            ruido += Convert.ToInt32(MicInputDb.MicLoudnessinDecibels);
            count++;
            if(count==90)
            {
                if(ruido/count>-150)
                {
                    AudioManager.instance.PlaySound("sh");
                }
                else
                {
                    AudioManager.instance.PlaySound("noTeEscucho");
                }
            }

            if(MicInputDb.MicLoudnessinDecibels>-90)
            {
                AudioManager.instance.PlaySound("sh");
            }

        yield return new WaitForSeconds(5f);
    }

    public IEnumerator FadeEnding()
    {
        yield return new WaitForSeconds(8f);
        FadeOut();
        yield return new WaitForSeconds(10f);
        CargarMenu();

    }

    public void Resume()
    {
        PauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        cc.enabled = true;
    }

    public void PauseGame()
    {
        PauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        cc.enabled = false;

    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        WebcamCapture.instance._webCamTexture.Stop();
        SceneManager.LoadScene(0);
    }

    public void Save()
    {
        DatosGuardados.instance.playerPosition = player.transform.localPosition;
        DatosGuardados.instance.key = key;
        GestorDePersistencia.instancia.GuardarEstadoJuego();
    }

    public void Load()
    {
        if(DatosGuardados.instance.load)
        {
            //player.transform.localPosition = DatosGuardados.instance.playerPosition;
            key = DatosGuardados.instance.key;
            player.transform.localPosition = DatosGuardados.instance.playerPosition;
            for (int i = 0; i < key; i++)
            {
                keys[i].gameObject.SetActive(true);
            }
        }
    }

    public void Screens()
    {
        foreach (GameObject item in screen)
        {
            WebcamCapture.instance._webCamTexture.requestedHeight = 512;
            WebcamCapture.instance._webCamTexture.requestedWidth = 512;
            item.GetComponent<MeshRenderer>().material = WebcamCapture.instance.gameObject.GetComponent<MeshRenderer>().material;
            item.GetComponent<MeshRenderer>().material.SetTexture("_WebcamTex", WebcamCapture.instance._webCamTexture);
        }
    }

    public void FadeOut()
    {
        fadeOut.SetActive(true);
    }

    public void CargarMenu()
    {
        SceneManager.LoadScene(0);
    }   


    public IEnumerator Test()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(20f, 50f));
        Debug.Log("corrutina corriendo");
        int intentos= 0;
        while (true)
        {
            
            if (!isPaused)
            {
                if (valores.Count == 0)
                {
                    yield return new WaitForSeconds(UnityEngine.Random.Range(3f, 5f));
                    Debug.Log("corrutina esperando");
                }

                Debug.Log("empezando a acumular");
                valores.Add(MicInputDb.MicLoudnessinDecibels);
                if (valores.Count > 15)
                {
                    float acumulacion = 0;
                    foreach (var item in valores)
                    {
                        acumulacion += item;
                    }

                    acumulacion = acumulacion / valores.Count;

                    valores.Clear();

                    if (acumulacion > -120)
                    {
                        AudioManager.instance.PlaySound("sh");
                        yield return new WaitForSeconds(UnityEngine.Random.Range(10f, 20f));
                        intentos = 0;
                    }
                    else if(acumulacion <-170)
                    {
                        intentos++;
                        if(intentos==3)
                        {
                            AudioManager.instance.PlaySound("noTeEscucho");
                            intentos = 0;
                            yield return new WaitForSeconds(UnityEngine.Random.Range(10f, 20f));
                        }
                        
                        yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 5f));
                    }
                }
            }
            
        }
        
    }


    public IEnumerator Test2()
    {
        float time = 0;
        int x = UnityEngine.Random.Range(UnityEngine.Random.Range(8, 18), UnityEngine.Random.Range(19, 2));
        yield return new WaitForSeconds(15);
        while (true)
        {
            if (puzzle.gameObject.GetComponent<Puzzle>().puzzleSolved)
            {
                break;
            }
            if (puzzleOpened && !puzzle.gameObject.GetComponent<Puzzle>().puzzleSolved)
            {
                Debug.Log(x);
                while (true)
                {
                    yield return new WaitForSeconds(0.01f);
                    time += Time.deltaTime;
                    if (MicInputDb.MicLoudnessinDecibels < -165 && time >= x && x % 2 == 0)
                    {
                        Debug.Log(MicInputDb.MicLoudnessinDecibels);
                        //AudioManager.instance.PlaySound("noTeEscucho");
                        yield return new WaitForSeconds(UnityEngine.Random.Range(3f, 5f));
                        time = 0;
                        x = UnityEngine.Random.Range(UnityEngine.Random.Range(8, 18), UnityEngine.Random.Range(19, 25));
                    }
                    if (MicInputDb.MicLoudnessinDecibels < -165 && time >= x && x % 2 == 1)
                    {
                        x = UnityEngine.Random.Range(UnityEngine.Random.Range(8, 18), UnityEngine.Random.Range(19, 25));
                        time = 0;
                    }
                    if (MicInputDb.MicLoudnessinDecibels > -120)
                    {
                        Debug.Log(MicInputDb.MicLoudnessinDecibels);
                        AudioManager.instance.PlaySound("sh");
                        yield return new WaitForSeconds(UnityEngine.Random.Range(3f, 5f));
                        x = UnityEngine.Random.Range(UnityEngine.Random.Range(8, 18), UnityEngine.Random.Range(19, 25));
                        time = 0;
                    }
                    if (puzzle.gameObject.GetComponent<Puzzle>().puzzleSolved)
                    {
                        break;
                    }
                }
            }
            else
            {
                if (!isPaused)
                {


                    //yield return new WaitForSeconds(15);
                    time = 0;
                    x = UnityEngine.Random.Range(15, 28);
                    Debug.Log(x);
                    while (true)
                    {
                        yield return new WaitForSeconds(0.01f);
                        time += Time.deltaTime;
                        if (MicInputDb.MicLoudnessinDecibels < -165 && time >= x)
                        {
                            Debug.Log(MicInputDb.MicLoudnessinDecibels);
                            //AudioManager.instance.PlaySound("noTeEscucho");
                            yield return new WaitForSeconds(UnityEngine.Random.Range(3f, 5f));
                            time = 0;
                            x = UnityEngine.Random.Range(15, 30);
                        }
                        if (MicInputDb.MicLoudnessinDecibels > -120)
                        {
                            Debug.Log(MicInputDb.MicLoudnessinDecibels);
                            AudioManager.instance.PlaySound("sh");
                            yield return new WaitForSeconds(UnityEngine.Random.Range(3f, 5f));
                            time = 0;
                            x = UnityEngine.Random.Range(15, 30);
                        }
                        if (puzzle.gameObject.GetComponent<Puzzle>().puzzleSolved)
                        {
                            break;
                        }
                        if (puzzleOpened)
                        {
                            break;
                        }
                    }

                }
            }

        }


    }

    IEnumerator WriteText(TextMeshProUGUI text, float limit)
    {
        cutoff = text.fontMaterial.GetFloat("_Cutoff");
        while(cutoff>limit)
        {
            cutoff -= Time.deltaTime;
            text.fontMaterial.SetFloat("_Cutoff",cutoff);
            yield return new WaitForSeconds(0.1f);
        }
        

    }

    public IEnumerator Alarm()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(60f, 120f));
        clock.GetComponent<Animator>().enabled = true;
        clock.GetComponent<AudioSource>().Play();
    }

}