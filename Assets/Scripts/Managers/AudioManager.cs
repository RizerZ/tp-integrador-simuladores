﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;
    public static AudioManager instance;
    public Sound[] Relampagos1;
    public Sound[] Relampagos2;
    public int ran = 0;
    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sound s in Relampagos1)
        {
            s.Gameobject = gameObject.AddComponent<AudioSource>();
            s.Gameobject.clip = s.Clip;
            s.Gameobject.volume = s.Volume;
            s.Gameobject.pitch = s.Pitch;
            s.Gameobject.loop = s.Loop;
        }

        foreach (Sound s in Relampagos2)
        {
            s.Gameobject = gameObject.AddComponent<AudioSource>();
            s.Gameobject.clip = s.Clip;
            s.Gameobject.volume = s.Volume;
            s.Gameobject.pitch = s.Pitch;
            s.Gameobject.loop = s.Loop;
        }

        foreach (Sound s in sounds)
        {
            s.Gameobject = gameObject.AddComponent<AudioSource>();
            s.Gameobject.clip = s.Clip;
            s.Gameobject.volume = s.Volume;
            s.Gameobject.pitch = s.Pitch;
            s.Gameobject.loop = s.Loop;
        }
    }
    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, x => x.Name == name);
        if (s == null)
        {
            Debug.Log("Sonido " + name + " no encontrado.");
            return;
        }
        else
        {
            s.Gameobject.Play();
        }
    }
    public void PlaySound(string name, ulong delay)
    {
        Sound s = Array.Find(sounds, x => x.Name == name);
        if (s == null)
        {
            Debug.Log("Sonido " + name + " no encontrado.");
            return;
        }
        else
        {
            s.Gameobject.Play(delay);
        }
    }
    public void PlayRandomSound()
    {
        var random = UnityEngine.Random.Range(0, 10)+1;
        if(ran==random)
        {
            if(random==11)
            {
                random--;
            }
            else if (random == 1)
            {
                random++;
            }
            else
            {
                random += (UnityEngine.Random.Range(-1, 1));
            }
        }
        string r = "random";
        string name = r + random.ToString();
        ran = random;
        Sound s = Array.Find(sounds, x => x.Name == name);
        if (s == null)
        {
            Debug.Log("Sonido " + name + " no encontrado.");
            return;
        }
        else
        {
            s.Volume = (UnityEngine.Random.Range(0.4f, 1f));
            s.Gameobject.Play();
        }
    }

    public void PlayRandomSound(float pitch)
    {
        var random = UnityEngine.Random.Range(0, 10) + 1;
        if (ran == random)
        {
            if (random == 11)
            {
                random--;
            }
            else if (random == 1)
            {
                random++;
            }
            else
            {
                random += (UnityEngine.Random.Range(-1, 1));
            }
        }
        string r = "random";
        string name = r + random.ToString();
        ran = random;
        Sound s = Array.Find(sounds, x => x.Name == name);
        if (s == null)
        {
            Debug.Log("Sonido " + name + " no encontrado.");
            return;
        }
        else
        {
            s.Pitch = pitch;
            s.Volume = (UnityEngine.Random.Range(0.4f, 1f));
            s.Gameobject.Play();
        }
    }

    public void PauseSound(string name)
    {
        
        Sound s = Array.Find(sounds, x => x.Name == name);
        if (s == null)
        {
            Debug.Log("Sonido " + name + " no encontrado.");
            return;
        }
        else
        {
            s.Volume = (UnityEngine.Random.Range(0.3f, 1f));
            s.Gameobject.Pause();
        }
    }
    public void PauseAll()
    {
        foreach (Sound s in sounds)
        {
            s.Gameobject.Pause();
        }
    }

    public void ReproducirSonidoR1(string nombre)
    {
        Sound s = Array.Find(Relampagos1, x => x.Name == nombre);
        if (s == null)
        {
            Debug.Log("Sonido " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.Volume=(UnityEngine.Random.Range(0.3f, 1f));
            s.Gameobject.Play();
        }
    }
    public void ReproducirSonidoR2(string nombre)
    {
        Sound s = Array.Find(Relampagos2, x => x.Name == nombre);
        if (s == null)
        {
            Debug.Log("Sonido " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.Volume = (UnityEngine.Random.Range(0.3f, 1f));
            s.Gameobject.Play();
        }
    }

    public IEnumerator Sounds()
    {
        while (true)
        {
            yield return new WaitForSeconds(UnityEngine.Random.Range(2.5f, 8f));
            AudioManager.instance.PlayRandomSound();
            yield return new WaitForSeconds(UnityEngine.Random.Range(2f, 15f));
            AudioManager.instance.PlayRandomSound();

        }

    }
}
