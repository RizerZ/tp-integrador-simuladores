﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CreateTXTFiles : MonoBehaviour
{
    public string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\This is not a game";
    public static CreateTXTFiles instance;
    string[] lines = { "Primera Línea", "Segunda Línea", "Tercera Línea" };
    int count;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\This is not a game";
        }/*
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);*/
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SaveTXT();
        }
    }


    public void SaveTXT()
    {
        count++;
        if (!Directory.Exists(desktopPath))
        {
            Directory.CreateDirectory(desktopPath);
        }
        System.IO.File.WriteAllLines(desktopPath+"\\"+count.ToString()+".txt", lines);
    }
}
