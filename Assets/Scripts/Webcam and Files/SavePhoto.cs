﻿using System;
using System.IO;
using UnityEngine;

public class SavePhoto : MonoBehaviour
{
    private Texture2D snap;
    public string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +"\\HorrorGame";

    public static SavePhoto instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\HorrorGame";
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }


    public void TakePhoto()
    {
        snap = new Texture2D(WebcamCapture.instance._webCamTexture.width, WebcamCapture.instance._webCamTexture.height);
        snap.SetPixels(WebcamCapture.instance._webCamTexture.GetPixels());
        snap.Apply();
        if (!Directory.Exists(desktopPath)) //+ "\\HorrorGame"))
        {
            Directory.CreateDirectory(desktopPath); //+ "\\HorrorGame");
        }
        System.IO.File.WriteAllBytes(desktopPath+"\\" /*+ "\\HorrorGame\\"*/ + DateTime.Now.ToString("dd-MM-yyyy")+" " +DateTime.Now.ToString("HH-mm-ss") +".png", snap.EncodeToPNG());

        GetFiles.instance.GetFilesInPath(desktopPath, "*.png");
    }
    

}
