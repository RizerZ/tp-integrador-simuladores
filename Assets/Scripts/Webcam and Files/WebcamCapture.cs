﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Windows.WebCam;

public class WebcamCapture : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer meshrenderer;

    public WebCamTexture _webCamTexture;
    public bool webCam;
    public static WebcamCapture instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;


            if (_webCamTexture == null)
            {
                _webCamTexture = new WebCamTexture();
            }
            if (WebCamTexture.devices.Length > 0)
            {
                webCam = true;
            }
            else
            {
                webCam = false;
            }

        }
    }
    private void Start()
    {

        WebCamDevice[] camDevices = WebCamTexture.devices;
        for (int i = 0; i < camDevices.Length; i++)
        {
            Debug.Log("Webcam available: " + camDevices[i].name);
        }
        if (webCam)
        {
            _webCamTexture.Play();
            
        }

        meshrenderer = GetComponent<MeshRenderer>();

        _webCamTexture.requestedHeight = 512;
        _webCamTexture.requestedWidth = 512;

    }
    private void Update()
    {

        meshrenderer.material.SetTexture("_WebcamTex", _webCamTexture);
        /*
        if(Input.GetKeyDown(KeyCode.P))
        {
            SavePhoto.instance.TakePhoto();
        }*/
    }
}