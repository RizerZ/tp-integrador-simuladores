﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GetFiles : MonoBehaviour
{
    private string path;
    private string[] files;

    public static GetFiles instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }/*
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);*/
    }

    // Start is called before the first frame update
    void Start()
    {
        path = SavePhoto.instance.desktopPath;
        GetFilesInPath(path, "*png");
    }

    // Update is called once per frame
    void Update()
    {

    }


    public void GetFilesInPath(string _path, string pattern)
    {
        files=Directory.GetFiles(_path, pattern);
    }

    public Texture2D GetTextureFromPhoto()
    {
        if(path==null)
        {
            path=SavePhoto.instance.desktopPath;
        }
        if(files is null)
        {
            GetFilesInPath(path, "*.png");
        }
        if(files.Length>0)
        {
            var texture = System.IO.File.ReadAllBytes(files[Random.Range(0, files.Length)]);
            Texture2D tex = new Texture2D(512, 512);
            tex.LoadImage(texture);
            return tex;
        }
        else
        {
            return null;
        }
        
    }
}
